#ifndef LEVEL_H
#define	LEVEL_H

#include <iostream>
#include "raylib.h"
#include "blocks.h"
#include "player.h"
#include "ball.h"
#include "buttons.h"
#include "play.h"

namespace arkanoid {

	namespace levels {

		class Level {
		private:
			int fixedBlocksL1;
			int fixedBlocksL2;
			int fixedBlocksL3;
			int blocksLeftL1;
			int blocksLeftL2;
			int blocksLeftL3;
			int levelCounter;			
		public:
			Level();
			void setBlocksLeftL1();
			void setBlocksLeftL2();
			void setBlocksLeftL3();

			void deductBlocksL1();
			void deductBlocksL2();
			void deductBlocksL3();

			int getBlocksLeftL1();
			int getBlocksLeftL2();
			int getBlocksLeftL3();

			void addlevelCounter();
			int getlevelCounter();

			void resetLevelNumber();
			void resetAll();	
		};

		enum class levelNumber {
			levelOne = 1,
			levelTwo,
			levelThree
		};

		//extern const int levelOneBlocksCols;
		//extern const int levelOneBlocksRows;	

		extern levelNumber currentLevel;
		extern Level* myLevel;
		extern int blocksLeft;
		extern int levelCounter;

		extern float contYL1;
		extern float contXL1; //reset esta var

		void initLevelOne();
		void initLevelTwo();
		void initLevelThree();

		void updateLevelOne();
		void updateLevelTwo();
		void updateLevelThree();

		void drawLevelOne();
		void drawLevelTwo();
		void drawLevelThree();

		void unsetCollisionLevelOne();
		void unsetCollisionLevelTwo();
		void unsetCollisionLevelThree();

		void deInitLevelOne();
		void deInitLevelTwo();
		void deInitLevelThree();

		void resetLevelNumber();

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif 

