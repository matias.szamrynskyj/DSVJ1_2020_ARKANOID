#ifndef BLOCKS_H
#define BLOCKS_H

#include <iostream>
#include "raylib.h"
#include "ball.h"

namespace arkanoid {

	namespace blocks {

		class Blocks{
		private:
			float witdh;
			float height;
			Vector2 size;
			Color color;
			Rectangle block;
			bool collided;		
			int blockLives;
			
		public:
			Blocks();
			~Blocks();
			void setWitdh(float witdh);
			void setHeight(float height);
			void setPos(float x, float y);
			void setColor(Color _color);
			void setCollision();			
			void setBlockTolerance(int lives);	

			float getWitdh();
			float getHeight();
			float getX();
			float getY();
			Rectangle getBlock();						//Retorna el boton en s�
			Color getColor();
			bool getCollision();		

			void drawBlock();
			void drawBlock(Color color);

			void damageBlock();
			void unsetCollision();
			int getBlockLives();
		};

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
