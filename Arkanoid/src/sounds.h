#ifndef	SOUNDS_H
#define SOUNDS_H

#include "raylib.h"

namespace arkanoid {

	namespace sound {

		class sounds {
		private:
			Sound gameSound;

		public:
			sounds(const char* fileName);
			void setSound(const char* fileName);
			void playSound();
			void setVolumen();
		};

		extern sounds* buttonSFX;
		extern sounds* racketCollisionSFX;
		extern sounds* destroyedBrickSFX;
		extern sounds* outOfBoundsSFX;
		extern sounds* wallCollisionSFX;
		extern sounds* winSFX;
		extern sounds* lostSFX;
		

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif

