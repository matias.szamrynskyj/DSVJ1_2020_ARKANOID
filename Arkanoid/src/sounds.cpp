#include "sounds.h"

namespace arkanoid {

	namespace sound {


		sounds* buttonSFX;
		sounds* racketCollisionSFX;
		sounds* destroyedBrickSFX;
		sounds* outOfBoundsSFX;
		sounds* wallCollisionSFX;
		sounds* winSFX;
		sounds* lostSFX;

		void init() {
			InitAudioDevice();
			buttonSFX = new sounds("res/SFX/clickSound.wav");
			racketCollisionSFX = new sounds("res/SFX/racketCollisionSound.wav");
			destroyedBrickSFX = new sounds("res/SFX/destroyedBrick.wav");
			outOfBoundsSFX = new sounds("res/SFX/ballOutOfBounds.wav");
			wallCollisionSFX = new sounds("res/SFX/wallCollision.wav");
			winSFX = new sounds("res/SFX/winSound.wav");
			lostSFX = new sounds("res/SFX/lostSound.wav");
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {
			delete buttonSFX;
			delete racketCollisionSFX;
			delete destroyedBrickSFX;
			delete outOfBoundsSFX;
			delete wallCollisionSFX;
			delete winSFX;
			delete lostSFX;

			CloseAudioDevice();
		}

		
		sounds::sounds(const char* fileName){
			setSound(fileName);
		}

		void sounds::setSound(const char* fileName)	{
			gameSound = LoadSound(fileName);
		}
		void sounds::playSound()
		{
			PlaySound(gameSound);
		}
		void sounds::setVolumen()
		{
			SetSoundVolume(gameSound, 1.0f);
		}
	}
}