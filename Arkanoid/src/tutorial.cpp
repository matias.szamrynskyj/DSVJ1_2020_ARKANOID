#include "tutorial.h"

using namespace arkanoid;
using namespace buttons;
using namespace menu;

namespace arkanoid {

	namespace tutorial {

		float controlSizeHeight;
		float controlSizeWidth;
		int controlsTextPosX;
		int controlsTextPosY;
		Vector2 controlsButtonSize;


		char playerLeftKeyText[2] = "A";
		char playerRightKeyText[2] = "D";
		float playerKeyFont;

		float playerLeftKeyPosX;
		float playerLeftKeyPosY;

		float playerRightKeyPosX;
		float playerRightKeyPosY;

		Vector2 playerLeftKeyTextPos;
		Vector2 playerRightKeyTextPos;

		int goLeftTextPosY;
		int goLeftTextPosX;

		int goRightTextPosX;
		int goRightTextPosY;


		Buttons* spacebarButton;
		float spaceSizeWidth;
		float spaceSizeHeight;
		float spacebarPosX;
		float spacebarPosY;
		Vector2 spacebarButtonSize;
		Vector2 spacebarTextPos;
		char spacebarText[9] = "SPACEBAR";
		float spacebarFont;
		int spacebarTextPosX;
		int spacebarTextPosY;

		void init() {
			//para el tama�o del recuadro de los botones A y D
			controlSizeHeight = 100;
			controlSizeWidth = 100;
			controlsTextPosX = GetScreenWidth() / 2 - 80;
			controlsTextPosY = 30;
			controlsButtonSize = { controlSizeWidth, controlSizeHeight };

			//Posicion para el boton A
			playerLeftKeyPosX = GetScreenWidth()/2 + controlSizeWidth/2;
			playerLeftKeyPosY = 150.0f;

			//Posicion para el boton D
			playerRightKeyPosX = playerLeftKeyPosX - 200.0f;
			playerRightKeyPosY = playerLeftKeyPosY;

			//Setear posiciones de los textos
			playerLeftKeyTextPos = { playerLeftKeyPosX + 25 , playerLeftKeyPosY + 15 };
			playerRightKeyTextPos = { playerRightKeyPosX + 28, playerRightKeyPosY + 18 };

			//Posicion de texto de Go Left
			goLeftTextPosX = GetScreenWidth()/2 - 155;
			goLeftTextPosY = 110;

			//Posicion de texto de Go Right
			goRightTextPosX = goLeftTextPosX + 195;
			goRightTextPosY = goLeftTextPosY;

			//Spacebar Init
			spaceSizeWidth = 260.0f;
			spaceSizeHeight = 65.0f;
			spacebarPosX = GetScreenWidth() / 2 - spaceSizeWidth/2;
			spacebarPosY = GetScreenHeight() / 2 + 50;
			spacebarButtonSize = { spaceSizeWidth , spaceSizeHeight};
			spacebarTextPos = { spacebarPosX + 10.0f , spacebarPosY + 18.0f };
			spacebarFont = 35.0f;
			spacebarTextPosX = GetScreenWidth()/2 - 80;
			spacebarTextPosY = GetScreenHeight()/2 + 10;

			playerLeftKey = new Buttons(playerLeftKeyPosX, playerLeftKeyPosY, controlsButtonSize, defaultColor);
			playerRightKey = new Buttons(playerRightKeyPosX, playerRightKeyPosY, controlsButtonSize, defaultColor);
			spacebarButton = new Buttons(spacebarPosX, spacebarPosY, spacebarButtonSize, defaultColor);

			playerLeftKey->setName("p1 Up Keys");
			playerRightKey->setName("p1 Down Keys");
			spacebarButton->setName("spacebar");
		}

		void update() {

			backButton->buttonLogic4(backButton, collisionColor, defaultColor, currentScene, menuScreen);
		}

		void draw() {
			DrawText(FormatText("CONTROLS"), controlsTextPosX, controlsTextPosY, 30, GREEN);


			DrawText(FormatText("Go Left"), goLeftTextPosX, goLeftTextPosY, 30, GREEN);
			DrawText(FormatText("Go Right"), goRightTextPosX, goRightTextPosY, 30, GREEN);
			DrawText(FormatText("Shoot Ball"), spacebarTextPosX, spacebarTextPosY, 30, GREEN);
						

			playerLeftKey->drawButton(RED);
			playerLeftKey->showText(font, playerLeftKeyText, playerLeftKeyTextPos, 75.0f, 5.0f, BLUE);

			playerRightKey->drawButton(RED);
			playerRightKey->showText(font, playerRightKeyText, playerRightKeyTextPos, 75.0f, 5.0f, BLUE);

			backButton->drawButton(backButton->getColors());
			backButton->showText(font, backText, backTextPos, backFontSize, 10.0f, textColor);

			spacebarButton->drawButton(RED);
			spacebarButton->showText(font, spacebarText, spacebarTextPos, spacebarFont, 10.0f, textColor);
		}

		void deinit() {
			delete playerLeftKey;
			delete playerRightKey;
			delete spacebarButton;
		}
	}
}