#include "game.h"

using namespace arkanoid;
using namespace scenes;

namespace arkanoid {

	namespace game {

		int screenWidth = 800;
		int screenHeight = 600;
		bool exitGame = false;

		void run() {
			init();
			while (!WindowShouldClose() && exitGame != true)
			{
				update();
				draw();
			}
			deinit();
		}

		static void init() {
			InitWindow(screenWidth, screenHeight, "ARKANOID");

			SetTargetFPS(60);
			SetExitKey(KEY_DELETE);

			currentScene = Scene::menu;	
			exitGame = false;	

			//InitAudioDevice();
			//sound::fxButton = LoadSound("res/SFX/clickSound.ogg");

			sound::init();
			menu::init();
			ball::init();
			player::init();
			buttons::init();
			play::init();
			scenes::init();
			tutorial::init();	
			levels::init();
			victoryWindow::init();
			lostWindow::init();
		}

		static void update() {
			switch (currentScene)
			{
			case Scene::menu:
				menu::update();
				break;

			case Scene::play:
				play::update();
				break;

			case Scene::tutorial:
				tutorial::update();
				break;

			case Scene::credits:
				credits::update();
				break;

			case Scene::quit:
				quit::update();
				break;

			case Scene::winScene:
				victoryWindow::update();
				break;

			case Scene::lostScene:
				lostWindow::update();
				break;
			}
		}

		static void draw() {
			BeginDrawing();
			ClearBackground(BLACK);

			switch (currentScene)
			{
			case Scene::menu:
				menu::draw();
				break;

			case Scene::play:
				play::draw();
				break;

			case Scene::tutorial:
				tutorial::draw();
				break;

			case Scene::credits:
				credits::draw();
				break;

			case Scene::quit:
				quit::draw();
				break;

			case Scene::winScene:
				victoryWindow::draw();
				break;

			case Scene::lostScene:
				lostWindow::draw();
				break;
			}
			EndDrawing();
		}

		static void deinit() {
			ball::deinit();
			player::deinit();
			buttons::deinit();
			levels::deinit();
			sound::deinit();
			CloseWindow();
		}
	}
}