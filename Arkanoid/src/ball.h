#ifndef BALL_H
#define BALL_H

#include <iostream>
#include "raylib.h"
#include "player.h"
#include "sounds.h"


namespace arkanoid {

	namespace ball {

		class Ball {
		private:
			const Vector2 fixedBallPosition = { (float)GetScreenWidth() / 2, (float)GetScreenHeight() / 2 + 180};
			const Vector2 fixedBballSpeed = { -9.0f, -9.0f };
			const float fixedRadius = 10.0f;
			const Color fixedColor = RED;

			Vector2 top;
			Vector2 bottom;

			Vector2 ballPosition;
			Vector2 ballSpeed;
			float ballRadius;
			Color ballColor;
			bool shoot;

		public:
			Ball();
			Ball(Vector2 speed, Color color);
			Ball(Vector2 speed, float radius, Color color);
			~Ball();

			void setBallSpeed(Vector2 speed);                      //Setea la velocidad de la pelota
			void setRadius(float radius);                          //Setea el radio de la pelota
			void movement();                                       //Ejecuta el movimiento de la pelota
			void setCollisionX();                                  //Colision y cambio de direccion en X
			void setCollisionY();                                  //Colision y cambio de direccion en Y
			void setColor(Color color);                            //Seteo el color de la pelota
			void setNewPos(float x, float y);                      //Seteo el color de la pelota
			void setShoot();
			void setShootFalse();

			float getRadius();                                     //Retorna el radio de la pelota
			float getX();                                          //Retorna la posicion en X de la pelota
			float getY();                                          //Retorna la posicion en Y de la pelota
			float getSpeedX();                                     //Retorna la velocidad en X de la pelota
			float getSpeedY();                                     //Retorna la velocidad en X de la pelota
			Vector2 getXY();                                       //Retorna la posicion en X e Y de la pelota
			Vector2 getOriginalPos();
			Vector2 getTop(Ball* gameBall);
			Vector2 getBottom(Ball* gameBall);
			void getBallSpeedX(float speed);	

			void drawBall();                                       //Dibuja la pelota en pantalla

			void resetBallPos();                                //Resetea la pelota si se va de los l�mites de la pantalla			                             
			void moveX(float speed);
			bool wasShot();										   //Chequea si el jugador apreto espacio para disparar la pelota
		};

		extern float ballSpeedX;
		extern float ballSpeedY;
		extern Vector2 ballSpeed;
		extern Color ballColor;
		extern Ball* gameBall;
		extern bool impact;				//este flag es para que la pelota no clipee con la paleta al tocarla
		extern bool impact2;			//impact 2 es flag de la pelota contra las paredes

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif




