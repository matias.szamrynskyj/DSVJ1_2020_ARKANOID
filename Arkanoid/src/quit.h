#ifndef QUIT_H
#define QUIT_H

#include "buttons.h"
#include "menu.h"
#include "scenes.h"
#include "game.h"

namespace arkanoid {

	namespace quit {

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif

