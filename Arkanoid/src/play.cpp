#include "play.h"

using namespace arkanoid;
using namespace player;
using namespace ball;
using namespace buttons;
using namespace menu;
using namespace blocks;

namespace arkanoid {

	namespace play {

		bool pause;
		bool playing;		
		bool activateTimerpowerUp; //Activa el timer que cuenta los segundos que el powerUp aparece en pantalla
		
		//Game timer

				
		int colorTextPosX;
		int colorTextPosY;

		float racketSpeedPowerUpP1 = 25.0f;		

		void input() {

		}

		void gameCollisions() {
			//Check Collision ground
			if ((gameBall->getY() >= (GetScreenHeight() - gameBall->getRadius())) || (gameBall->getY() <= gameBall->getRadius())) {
				
			}

			//Check Collision player
			if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), playerRacket->getRacket())) {
				if (gameBall->getSpeedX() > 0) {
					gameBall->setCollisionX();
				}			
			}

			//Collision en Y
			if (CheckCollisionPointRec(gameBall->getTop(gameBall), playerRacket->getRacket()) ||
				CheckCollisionPointRec(gameBall->getBottom(gameBall), playerRacket->getRacket())){
				
				gameBall->setCollisionY();
			}
		}


		void init() {
			pause = false;
			playing = false;

			colorTextPosX = 275;
			colorTextPosY = 20;	
		}

		void update() {	
				if (IsKeyPressed(KEY_ESCAPE)) {
					pause = !pause;
				}

				if (!pause) {
					player::update();
					ball::update();
					blocks::update();
					levels::update();
				}
				else {
					exitButton->changePos(exitPausePos);
					exitButton->buttonLogic4(exitButton, collisionColor, defaultColor, currentScene, menuScreen);
					
					if (currentScene == menuScreen) {
						exitButton->setOriginalPos();
						playing = false;
						pause = !pause;
						//Meter todo esto en una funcion
						playerRacket->resetLives();						
						levels::myLevel->resetAll();
						gameBall->resetBallPos();
						gameBall->setShootFalse();
						playerRacket->resetRacketPos();
						//-------------------------------
					}
				}
		}

		void draw() {	
				if (!pause) {

					ball::draw();
					player::draw();
					blocks::draw();
					levels::draw();

					DrawText("PRESS ESCAPE to PAUSE GAME", 10, GetScreenHeight() - 25, 20, textColor);
				}
				else {
					DrawText("Arkanoid", (int)arkanoidLogoPosX, (int)arkanoidLogoPosY, 40, RED);
					exitButton->drawButton(exitButton->getColors());
					exitButton->showText(font, exitText, exitTextPausePos, quitFontSize, 10.0f, textColor);

					DrawText("PRESS ESCAPE to UNPAUSE", 10, GetScreenHeight() - 25, 20, textColor);
				}
				DrawFPS(10, 10);
		}

		void deinit() {

		}
	}
}