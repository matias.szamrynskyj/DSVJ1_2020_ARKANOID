#include "lostWindow.h"

using namespace arkanoid;
using namespace buttons;
using namespace menu;
using namespace play;
using namespace player;
using namespace ball;
using namespace levels;

namespace arkanoid {

	namespace lostWindow {

		int textPosX;
		int textPosY;

		int playerPointsPosX;
		int playerPointsPosY;

		int lostAtLevelPosX;
		int lostAtLevelPosY;

		float lostExitPosX;
		float lostExitPosY;
		Vector2 lostExitPos;

		void init() {
			textPosX = GetScreenWidth() / 2 - 80;
			textPosY = GetScreenHeight() / 2 - 250;

			playerPointsPosX = GetScreenWidth() / 2 - 150;
			playerPointsPosY = GetScreenHeight() / 2 - 150;

			lostAtLevelPosX = GetScreenWidth() / 2 - 150;
			lostAtLevelPosY = GetScreenHeight() / 2 - 100;

			lostExitPosX = buttonMiddleX;
			lostExitPosY = 250.0f;

			lostExitPos = { lostExitPosX , lostExitPosY };
		}

		void update() {
			exitButton->changePos(lostExitPos);
			exitButton->buttonLogic4(exitButton, collisionColor, defaultColor, currentScene, menuScreen);

			if (currentScene == menuScreen) {
				exitButton->setOriginalPos();
				playing = false;				

				//Meter todo esto en una funcion
				playerRacket->resetLives();
				levels::myLevel->resetAll();
				gameBall->resetBallPos();
				gameBall->setShootFalse();
				playerRacket->resetRacketPos();
				//-------------------------------
			}
		}

		void draw() {
			exitButton->drawButton(exitButton->getColors());
			exitButton->showText(font, exitText, exitTextPausePos, quitFontSize, 10.0f, textColor);

			DrawText(FormatText("Total Points: %i", playerRacket->getPoints()), playerPointsPosX, playerPointsPosY, 40, YELLOW);
			DrawText(FormatText("Lost at Level: %i", myLevel->getlevelCounter() ), lostAtLevelPosX, lostAtLevelPosY, 40, YELLOW);
			DrawText("You Lost", textPosX, textPosY, 40, YELLOW);

		}

		void deinit() {

		}
	}
}