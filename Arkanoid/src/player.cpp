#include "player.h"

using namespace arkanoid;

namespace arkanoid {

	namespace player {

		Player::Player(Vector2 pos, Vector2 size, Color myColor) {
			racket.x = pos.x;
			racket.y = pos.y;
			racket.width = size.x;
			racket.height = size.y;
			color = myColor;
			points = 0;
			lives = fixedLives;
		}


		Player::~Player() {
			std::cout << "Se borro player" << std::endl;
		}

		void Player::moveLeft(int key) {
			if (racket.x > 0)
			{
				if (IsKeyDown(key)) racket.x -= racketSpeed * GetFrameTime();

				//Solo muevo la pelota con la raqueta cuando el jugador no aprete espacio
				if (ball::gameBall->wasShot() != true) {
					if (IsKeyDown(key)) ball::gameBall->moveX(-racketSpeed);
				}
			}
		}

		void Player::moveRight(int key) {
			if (racket.x < GetScreenWidth() - racket.width)
			{
				if (IsKeyDown(key)) racket.x += racketSpeed * GetFrameTime();

				if (ball::gameBall->wasShot() != true) {
					if (IsKeyDown(key)) ball::gameBall->moveX(racketSpeed);
				}
			}
		}

		void Player::setHeight(float height) {
			racket.height = height;
		}

		void Player::setWidth(float width) {
			racket.width = width;
		}

		void Player::setInitPos(Vector2 pos) {
			racket.x = pos.x;
			racket.y = pos.y;
		}

		void Player::setColor(Color color) {
			color = color;
		}

		void Player::setPoints() {
			points++;
		}

		float Player::getHeight() {
			return racket.height;
		}

		float Player::getWidth() {
			return racket.width;
		}

		float Player::getX() {
			return racket.x;
		}

		float Player::getY() {
			return racket.y;
		}

		Rectangle Player::getRacket() {
			return racket;
		}

		int Player::getPoints() {
			return points;
		}

		int Player::getLives()
		{
			return lives;
		}

		void Player::resetPlayerSettings(float& speed) {
			racket.height = fixedSize.y;
			speed = racketSpeed;
		}

		void Player::resetRacketPos() {
			racket.x = playerRacketPos.x;
			racket.y = playerRacketPos.y;
		}

		void Player::resetPoints() {
			points = 0;
		}

		void Player::resetLives() {
			lives = fixedLives;
		}

		void Player::deductLives() {
			lives--;
		}

		void Player::drawPlayer() {
			DrawRectangleRec(racket, color);
		}

		void Player::drawLives() {
			DrawText(FormatText("Lives: %i", lives), livePosX, livePosY, 25, GREEN);
		}

		void Player::drawPoints() {
			DrawText(FormatText("Points: %i", points), pointsPosX, pointsPosY, 25, GREEN);
		}

		Vector2 playerSize;

		Vector2 playerRacketPos;

		Vector2 playerRacketClonePos;

		float racketWidth;
		float raccketHeight;

		Player* playerRacket;

		Player* playerRacketClone;

		int livePosX;
		int livePosY;

		int pointsPosX;
		int pointsPosY;

		Vector2 livePosVector;
		Vector2 pointsPosVector;

		void init() {
			racketWidth = 90;
			raccketHeight = 20;

			playerSize = { racketWidth,  raccketHeight };

			playerRacketPos = { static_cast<float>(GetScreenWidth()) / 2 - racketWidth / 2, 500.0f };

			playerRacketClonePos = { 230.0f, 200.0f };

			playerRacket = new Player(playerRacketPos, playerSize, VIOLET);
			playerRacketClone = new Player(playerRacketClonePos, playerSize, YELLOW);

			livePosX = GetScreenWidth() - 150;
			livePosY = GetScreenHeight() - 30;
			livePosVector = { static_cast<float>(livePosX) , static_cast<float>(livePosY) };

			pointsPosX = GetScreenWidth() - 150;
			pointsPosY = GetScreenHeight() - 75;

			pointsPosVector = { static_cast<float>(pointsPosX) , static_cast<float>(pointsPosY) };

		}

		void update() {
			playerRacket->moveLeft(KEY_A);
			playerRacket->moveRight(KEY_D);
			
		}

		void draw() {
			playerRacket->drawPlayer();
			playerRacket->drawLives();
			playerRacket->drawPoints();

			if (playerRacket->getLives() == 0) {
				currentScene = Scene::lostScene;
			}
		}

		void deinit() {
			delete playerRacket;
			delete playerRacketClone;
		}
	}
}