#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "scenes.h"
#include "ball.h"
#include "player.h"
#include "menu.h"
#include "tutorial.h"
#include "play.h"
#include "credits.h"
#include "quit.h"
#include "blocks.h"
#include "levels.h"
#include "victoryWindow.h"
#include "lostWindow.h"
#include "sounds.h"

namespace arkanoid {

	namespace game {

		extern int screenWidth;
		extern int screenHeight;
		extern bool exitGame;

		void run();

		static void init();
		static void update();
		static void draw();
		static void deinit();
	}
}
#endif

