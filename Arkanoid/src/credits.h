#ifndef CREDITS_H
#define CREDITS_H

#include "Buttons.h"
#include "Menu.h"
#include "scenes.h"

namespace arkanoid {

	namespace credits {

		void init();
		void update();
		void draw();
		void deinit();

	}

}

#endif // !CREDITS_H


