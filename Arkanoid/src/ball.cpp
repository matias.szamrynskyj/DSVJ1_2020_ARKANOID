#include "ball.h"

using namespace arkanoid;
using namespace player;
using namespace ball;

namespace arkanoid {

	namespace ball {

		Ball::Ball()
			:ballPosition{ fixedBallPosition }, ballSpeed{ fixedBballSpeed }, ballRadius{ fixedRadius }, ballColor{ fixedColor }{
			top = { 0,0 };
			bottom = { 0,0 };
			shoot = false;
		}

		Ball::Ball(Vector2 speed, Color color)
			: ballPosition{ fixedBallPosition }, ballSpeed{ speed }, ballRadius{ fixedRadius }, ballColor{ color }{
			top = { 0,0 };
			bottom = { 0,0 };
			shoot = false;
		}

		Ball::Ball(Vector2 speed, float radius, Color color)
			: ballPosition{ fixedBallPosition }, ballSpeed{ speed }, ballRadius{ radius }, ballColor{ color }{
			top = { 0,0 };
			bottom = { 0,0 };
			shoot = false;
		}

		Ball::~Ball() {
			std::cout << "Se borro la pelota" << std::endl;
		}

		void Ball::setBallSpeed(Vector2 speed) {
			ballSpeed = speed;
		}

		void Ball::setRadius(float radius) {
			ballRadius = radius;
		}

		void Ball::movement() {
			ballPosition.x += ballSpeed.x * GetFrameTime();
			ballPosition.y += ballSpeed.y * GetFrameTime();
		}

		void Ball::setCollisionX() {
			ballSpeed.x *= -1.0f;
		}

		void Ball::setCollisionY() {
			ballSpeed.y *= -1.0f;
		}

		void Ball::setColor(Color color) {
			ballColor = color;
		}

		void Ball::setNewPos(float x, float y) {
			ballPosition.x = x;
			ballPosition.y = y;
		}

		void Ball::setShoot() {
			if (IsKeyDown(KEY_SPACE)) {
				shoot = true;
			}
		}

		void Ball::setShootFalse()	{
			shoot = false;
		}

		Vector2 Ball::getXY() {
			return ballPosition;
		}

		Vector2 Ball::getOriginalPos() {
			return fixedBallPosition;
		}

		Vector2 Ball::getTop(Ball* gameBall) {
			return top = { gameBall->getX(), (gameBall->getY() + gameBall->getRadius()) };
		}

		Vector2 Ball::getBottom(Ball* gameBall) {
			return bottom = { gameBall->getX(), (gameBall->getY() - gameBall->getRadius()) };
		}

		void Ball::getBallSpeedX(float speed)
		{
			ballSpeed.x = speed;
		}

		float Ball::getRadius() {
			return ballRadius;
		}

		float Ball::getX() {
			return ballPosition.x;
		}

		float Ball::getY() {
			return ballPosition.y;
		}

		float Ball::getSpeedX() {
			return ballSpeed.x;
		}

		float Ball::getSpeedY() {
			return ballSpeed.y;
		}

		void Ball::drawBall() {
			DrawCircleV(ballPosition, ballRadius, ballColor);
		}

		//Resets ball position
		void Ball::resetBallPos() {
			ballPosition = fixedBallPosition;	
		}

		void Ball::moveX(float speed) {
			ballPosition.x += speed * GetFrameTime();
		}

		bool Ball::wasShot() {
			return shoot;
		}

		float ballSpeedX;
		float ballSpeedY;
		Vector2 ballSpeed;
		Color ballColor;
		Ball* gameBall;
		bool impact;
		bool impact2;

		void init() {
			ballSpeedX = 400.0f;
			ballSpeedY = 400.0f;
			ballSpeed = { -ballSpeedX ,-ballSpeedY };
			ballColor = BEIGE;
			gameBall = new Ball(ballSpeed, ballColor);
			impact = false;
			impact2 = false; //Flag para la pared
		}

		//Crear otro flag para los costados impacto2 para ball y paredes

		void update() {

			ball::gameBall->setShoot();	
			
			if (ball::gameBall->wasShot() != false) {
				gameBall->movement();
			}			
			
			//Colision con paredes
			if ((gameBall->getX() >= (GetScreenWidth() - gameBall->getRadius())) || (gameBall->getX() <= gameBall->getRadius()) && impact2 != true) {
				sound::wallCollisionSFX->playSound();
				gameBall->setCollisionX();
				impact = false;
				impact2 = true;
			}

			//Colision con el techo
			if ((gameBall->getY() <= gameBall->getRadius())) {
				sound::wallCollisionSFX->playSound();
				gameBall->setCollisionY();
				impact = false;
			}

			//Reset si la pelota se va en Y para abajo
			if ((gameBall->getY() >= (GetScreenHeight() + gameBall->getRadius()))) {
				gameBall->setShootFalse();
				gameBall->resetBallPos();
				playerRacket->deductLives();
				playerRacket->resetRacketPos();
				impact2 = false;
				sound::outOfBoundsSFX->playSound();
			}

			//Colision entre pelota y la raqueta
			if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), playerRacket->getRacket()) && impact != true) {
				sound::racketCollisionSFX->playSound();

				gameBall->setCollisionY();
				impact = true;
				impact2 = false;

				//Dividir la paleta
				if (gameBall->getX() < playerRacket->getX() + playerRacket->getWidth() / 2) {
					gameBall->getBallSpeedX(-ballSpeedX);
				}
				else {
					gameBall->getBallSpeedX(ballSpeedX);
				}
			}

			//Para solucionar el problema que la pelota rebotee al costado
			if (impact2 == true)
			{
				impact2 = false;
				if (gameBall->getX() < 50) {
					gameBall->setNewPos(gameBall->getX() + 10, gameBall->getY());
				}
				else {
					gameBall->setNewPos(gameBall->getX() - 10, gameBall->getY());
				}
			}
		}

		void draw() {
			gameBall->drawBall();
		}

		void deinit() {
			delete gameBall;
		}
	}
}