#ifndef PLAY_H
#define PLAY_H

#include "raylib.h"
#include "player.h"
#include "ball.h"
#include "buttons.h"
#include "menu.h"
#include "blocks.h"
#include "levels.h"

namespace arkanoid {

	namespace play {

		extern bool pause;
		extern bool playing;		
		extern bool activateTimerpowerUp;

		//Game timer
		extern int timer;
		extern int timerSeconds;
		extern int AuxTimerSeconds;
		extern int timerMax;
		extern int activationTime;
		extern int colorTextPosX;
		extern int colorTextPosY;		

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif

