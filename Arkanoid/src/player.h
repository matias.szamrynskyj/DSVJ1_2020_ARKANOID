#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include "raylib.h"
#include "ball.h"
#include "scenes.h"
#include "levels.h"

namespace arkanoid {

	namespace player {

		class Player {
		private:
			const int fixedLives = 5;
			const Vector2 fixedSize = { 100, 20 };
			const Vector2 fixedPos = { 0,0 };
			const Color fixedColor = WHITE;			
			float racketSpeed = 600.0f;

			Color color;			
			Rectangle racket;		
			int points;
			int lives;
		public:			
			Player(Vector2 size, Vector2 pos, Color myColor);
			~Player();

			void moveLeft(int key);
			void moveRight(int key);

			void setHeight(float height);
			void setWidth(float width);
			void setInitPos(Vector2 pos);
			void setColor(Color color);			
			void setPoints();

			float getHeight();
			float getWidth();
			float getX();
			float getY();
			Rectangle getRacket();
			int getPoints();
			int getLives();

			void resetPlayerSettings(float& speed);
			void resetRacketPos();
			void resetPoints();
			void resetLives();
			void deductLives();		
			void drawPlayer();
			void drawLives();
			void drawPoints();

		};

		extern Vector2 playerSize;

		extern Vector2 playerRacketPos;

		extern Vector2 playerRacketClonePos;

		extern float racketWidth;    //Setea el ancho de los botones del juego
		extern float raccketHeight;   //Setea el alto de los botones del juego
			
		extern Player* playerRacket;
	
		extern Player* playerRacketClone;

		extern int livePosX;
		extern int livePosY;

		extern int pointsPosX;
		extern int pointsPosY;

		extern Vector2 livePosVector;
		extern Vector2 pointsPosVector;

		extern const char* text;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif

