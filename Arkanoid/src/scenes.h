#ifndef SCENES_H
#define SCENES_H

namespace arkanoid {

	namespace scenes {
		enum class Scene {
			menu = 0,
			play,
			tutorial,
			credits,
			pauseScreen,
			quit,
			winScene,
			lostScene
		};
		
		extern Scene currentScene;
		extern Scene menuScreen;
		extern Scene playScreen;
		extern Scene tutorialScreen;
		extern Scene creditsScreen;
		extern Scene pauseScreen;
		extern Scene exitScreen;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif

