#include "scenes.h"

namespace arkanoid {

	namespace scenes {

		Scene currentScene;
		Scene menuScreen;
		Scene playScreen;
		Scene tutorialScreen;
		Scene creditsScreen;
		Scene pauseScreen;
		Scene exitScreen;
		Scene winScreen;
		Scene lostScreen;

		void init() {
			menuScreen = Scene::menu;
			playScreen = Scene::play;
			tutorialScreen = Scene::tutorial;
			creditsScreen = Scene::credits;
			pauseScreen = Scene::pauseScreen;
			exitScreen = Scene::quit;
			winScreen = Scene::winScene;			
			lostScreen = Scene::lostScene;
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {

		}
	}
}