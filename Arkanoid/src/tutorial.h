#ifndef TUTORIAL_H
#define TUTORIAL_H

#include "buttons.h"


namespace arkanoid {

	namespace tutorial {

		extern int controlsTextPosX;
		extern int controlsTextPosY;

		extern buttons::Buttons* spacebarButton;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif 


