#include "blocks.h"

using namespace arkanoid;
using namespace ball;
using namespace std;
using namespace blocks;

namespace arkanoid {

	namespace blocks {

		Blocks::Blocks() {
			witdh = 55.0f;
			height = 20.0f;
			size = { witdh , height };

			block.x = 1;
			block.y = 1;
			block.width = witdh;
			block.height = height;
		
			color = BLUE;
			collided = false;		
			blockLives = 0;
		}

		Blocks::~Blocks() {
			
		}

		void Blocks::setWitdh(float witdh) {
			block.width = witdh;
		}

		void Blocks::setHeight(float height) {
			block.height = height;
		}

		void Blocks::setPos(float x, float y) {
			block.x = x;
			block.y = y;
		}

		void Blocks::setColor(Color _color) {
			color = _color;
		}

		void Blocks::setCollision()	{
			collided = true;
		}

		void Blocks::setBlockTolerance(int lives)	{
			blockLives = lives;

			switch (lives)
			{
			case 1:
				setColor(RED);
				break;

			case 2:
				setColor(VIOLET);
				break;

			case 3:
				setColor(YELLOW);
				break;
			}
		}

		float Blocks::getWitdh() {
			return witdh;
		}

		float Blocks::getHeight() {
			return height;
		}

		float Blocks::getX() {
			return block.x;
		}

		float Blocks::getY() {
			return block.y;
		}

		Rectangle Blocks::getBlock() {
			return block;
		}

		Color Blocks::getColor() {
			return color;
		}

		bool Blocks::getCollision()	{
			return collided;
		}

		void Blocks::drawBlock()	{
			DrawRectangleRec(block, color);
		}

		void Blocks::drawBlock(Color color) {
			DrawRectangleRec(block, color);
		}

		void Blocks::damageBlock()	{
			blockLives--;
		}

		void Blocks::unsetCollision()	{
			collided = false;
		}

		int Blocks::getBlockLives()	{
			return blockLives;
		}

		void init() {

		}

		void update() {	
			
		}

		void draw() {		
			
		}

		void deinit() {
		
		}
	}
}