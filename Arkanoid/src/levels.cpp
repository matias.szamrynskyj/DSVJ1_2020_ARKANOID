#include "levels.h"

using namespace arkanoid;
using namespace levels;
using namespace blocks;
using namespace ball;
using namespace std;

namespace arkanoid {

	namespace levels {

		levelNumber currentLevel;
		int blocksLeft;
		int levelCounter;
		Level* myLevel;

		const int sizeBlocksiL1 = 5;
		const int sizeBlocksJL1 = 13;

		const int sizeBlocksiL2 = 5;
		const int sizeBlocksJL2 = 6;

		const int sizeBlocksiL3 = 7;
		const int sizeBlocksJL3 = 8;

		Blocks* blocksLevelOne[sizeBlocksiL1][sizeBlocksJL1];
		Blocks* blocksLevelTwo[sizeBlocksiL2][sizeBlocksJL2];
		Blocks* blocksLevelThree[sizeBlocksiL3][sizeBlocksJL3];

		float contYL1;
		float contXL1;

		float contYL2;
		float contXL2;

		float contYL3;
		float contXL3;

		float witdhL1;
		float heightL1;

		float witdhL2;
		float heightL2;

		float witdhL3;
		float heightL3;

		int setLivesL2 = 3;

		int randNum;

		Level::Level() {
			fixedBlocksL1 = 0;
			fixedBlocksL2 = 0;
			fixedBlocksL3 = 0;
			blocksLeftL1 = 0;
			blocksLeftL2 = 0;
			blocksLeftL3 = 0;
			levelCounter = 1;			
		}

		void Level::setBlocksLeftL1() {
			blocksLeftL1++;
			fixedBlocksL1 = blocksLeftL1;
		}

		void Level::setBlocksLeftL2() {
			blocksLeftL2++;
			fixedBlocksL2 = blocksLeftL2;
		}

		void Level::setBlocksLeftL3() {
			blocksLeftL3++;
			fixedBlocksL3 = blocksLeftL3;
		}


		void Level::deductBlocksL1() {
			blocksLeftL1--;
		}

		void Level::deductBlocksL2() {
			blocksLeftL2--;

		}

		void Level::deductBlocksL3() {
			blocksLeftL3--;
		}



		int Level::getBlocksLeftL1()
		{
			return blocksLeftL1;
		}

		int Level::getBlocksLeftL2()
		{
			return blocksLeftL2;
		}

		int Level::getBlocksLeftL3()
		{
			return blocksLeftL3;
		}



		void Level::addlevelCounter()
		{
			levelCounter++;
		}

		int Level::getlevelCounter()
		{
			return levelCounter;
		}

		void Level::resetLevelNumber() {
			levelCounter = 1;
		}

		void Level::resetAll() {
			unsetCollisionLevelOne();
			unsetCollisionLevelTwo();
			unsetCollisionLevelThree();
			currentLevel = levels::levelNumber::levelOne;
			resetLevelNumber();
			blocksLeftL1 = fixedBlocksL1;
			blocksLeftL2 = fixedBlocksL2;
			blocksLeftL3 = fixedBlocksL3;
			player::playerRacket->resetPoints();
		}


		void initLevelOne() {
			for (int i = 0; i < sizeBlocksiL1; i++) {
				if (i != 0) {
					contYL1 += 30.0f;
				}
				else {
					contYL1 = 30.0f;
				}

				for (int j = 0; j < sizeBlocksJL1; j++) {
					if (j != 0) {
						contXL1 += 60.0f;
					}
					else {
						contXL1 = 12.0f;
					}
					blocksLevelOne[i][j] = new Blocks();
					blocksLevelOne[i][j]->setPos(contXL1, contYL1);
					myLevel->setBlocksLeftL1();
				}
				contXL1 = 50;
			}
		}

		void initLevelTwo() {

			contYL2 = 0.0f;
			contXL2 = 0.0f;

			witdhL2 = 110.0f;
			heightL2 = 30.0f;

			for (int i = 0; i < sizeBlocksiL2; i++) {
				if (i != 0) {
					contYL2 += 50.0f;
				}
				else {
					contYL2 = 30.0f;
				}

				for (int j = 0; j < sizeBlocksJL2; j++) {
					if (j != 0) {
						contXL2 += 120.0f;
					}
					else {
						contXL2 = 45.0f;
					}
					blocksLevelTwo[i][j] = new Blocks();
					blocksLevelTwo[i][j]->setWitdh(witdhL2);
					blocksLevelTwo[i][j]->setHeight(heightL2);
					blocksLevelTwo[i][j]->setPos(contXL2, contYL2);

					if (j % 2 == 0) {
						blocksLevelTwo[i][j]->setBlockTolerance(2);
					}
					else {
						blocksLevelTwo[i][j]->setBlockTolerance(1);
					}

					myLevel->setBlocksLeftL2();
				}
				contXL2 = 50;
			}
		}

		void initLevelThree() {

			witdhL3 = 80.0f;
			heightL3 = 25.0f;

			for (int i = 0; i < sizeBlocksiL3; i++) {
				if (i != 0) {
					contYL3 += 45.0f;
				}
				else {
					contYL3 = 30.0f;
				}

				for (int j = 0; j < sizeBlocksJL3; j++) {
					if (j != 0) {
						contXL3 += 100.0f;
					}
					else {
						contXL3 = 10.0f;
					}

					randNum = rand() % 5;

					blocksLevelThree[i][j] = new Blocks();
					blocksLevelThree[i][j]->setWitdh(witdhL3);
					blocksLevelThree[i][j]->setHeight(heightL3);

					myLevel->setBlocksLeftL3();

					blocksLevelThree[i][j]->setPos(contXL3, contYL3);
				}
				contXL3 = 50;
			}
		}


		void updateLevelOne() {

			for (int i = 0; i < sizeBlocksiL1; i++) {
				for (int j = 0; j < sizeBlocksJL1; j++) {

					if (blocksLevelOne[i][j]->getCollision() != true) {

						if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), blocksLevelOne[i][j]->getBlock())) {
							sound::destroyedBrickSFX->playSound();
							player::playerRacket->setPoints();
							gameBall->setCollisionY();
							blocksLevelOne[i][j]->setCollision();
							impact = false;
							impact2 = false;
							myLevel->deductBlocksL1();
							cout << "Se destruyo bloque: " << i << "|" << j << endl;
						}
					}
				}
			}
		}

		void updateLevelTwo() {

			for (int i = 0; i < sizeBlocksiL2; i++) {
				for (int j = 0; j < sizeBlocksJL2; j++) {

					if (blocksLevelTwo[i][j]->getCollision() != true) {
						if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), blocksLevelTwo[i][j]->getBlock())) {

							if (blocksLevelTwo[i][j]->getBlockLives() == 1)
							{
								sound::destroyedBrickSFX->playSound();
								player::playerRacket->setPoints();
								blocksLevelTwo[i][j]->setCollision();
								myLevel->deductBlocksL2();
								cout << "Se destruyo bloque: " << i << "|" << j << endl;
							}
							else if (blocksLevelTwo[i][j]->getBlockLives() == 2)
							{
								blocksLevelTwo[i][j]->setColor(RED);
								blocksLevelTwo[i][j]->damageBlock();
							}

							gameBall->setCollisionY();

							if (j % 2 == 0) {
								cout << blocksLevelTwo[i][j]->getBlockLives() << endl;
							}

							impact = false;
							impact2 = false;
						}
					}

				}
			}

		}

		void updateLevelThree() {

			for (int i = 0; i < sizeBlocksiL3; i++) {
				for (int j = 0; j < sizeBlocksJL3; j++) {

					if (blocksLevelThree[i][j]->getCollision() != true) {

						if (CheckCollisionCircleRec(gameBall->getXY(), gameBall->getRadius(), blocksLevelThree[i][j]->getBlock())) {
							sound::destroyedBrickSFX->playSound();
							player::playerRacket->setPoints();
							gameBall->setCollisionY();
							blocksLevelThree[i][j]->setCollision();
							myLevel->deductBlocksL3();
							cout << "Se destruyo bloque: " << i << "|" << j << endl;
							impact = false;
							impact2 = false;							
						}
					}
				}
			}

		}


		void drawLevelOne() {

			for (int i = 0; i < sizeBlocksiL1; i++) {
				for (int j = 0; j < sizeBlocksJL1; j++) {

					if (blocksLevelOne[i][j] != NULL) {
						if (blocksLevelOne[i][j]->getCollision() != true) {
							blocksLevelOne[i][j]->drawBlock(RED);
						}
					}

				}
			}

			#if DEBUG
			DrawText(FormatText("Bloques: %i", myLevel->getBlocksLeftL1()), 650, 500, 25, GREEN);
			#endif

			DrawText(FormatText("Level: %i", myLevel->getlevelCounter()), 650, 548, 25, GREEN);
		}

		void drawLevelTwo() {

			for (int i = 0; i < sizeBlocksiL2; i++) {
				for (int j = 0; j < sizeBlocksJL2; j++) {

					if (blocksLevelTwo[i][j] != NULL) {
						if (blocksLevelTwo[i][j]->getCollision() != true) {
							blocksLevelTwo[i][j]->drawBlock();
						}
					}

				}
			}
#if DEBUG
			DrawText(FormatText("Bloques: %i", myLevel->getBlocksLeftL2()), 650, 500, 25, GREEN);
#endif
			DrawText(FormatText("Level: %i", myLevel->getlevelCounter()), 650, 548, 25, GREEN);
		}

		void drawLevelThree() {

			for (int i = 0; i < sizeBlocksiL3; i++) {
				for (int j = 0; j < sizeBlocksJL3; j++) {

					if (blocksLevelThree[i][j] != NULL) {
						if (blocksLevelThree[i][j]->getCollision() != true) {
							blocksLevelThree[i][j]->drawBlock();
						}
					}

				}
			}
			#if DEBUG
			DrawText(FormatText("Bloques: %i", myLevel->getBlocksLeftL3()), 650, 500, 25, GREEN);
			#endif
			DrawText(FormatText("Level: %i", myLevel->getlevelCounter()), 650, 548, 25, GREEN);
		}


		void unsetCollisionLevelOne() {
			for (int i = 0; i < sizeBlocksiL1; i++) {
				for (int j = 0; j < sizeBlocksJL1; j++) {
					if (blocksLevelOne[i][j] != NULL) {
						blocksLevelOne[i][j]->unsetCollision();
					}
				}
			}
		}

		void unsetCollisionLevelTwo() {
			for (int i = 0; i < sizeBlocksiL2; i++) {
				for (int j = 0; j < sizeBlocksJL2; j++) {
					if (blocksLevelTwo[i][j] != NULL) {
						blocksLevelTwo[i][j]->unsetCollision();
					}
				}
			}
		}

		void unsetCollisionLevelThree() {
			for (int i = 0; i < sizeBlocksiL3; i++) {
				for (int j = 0; j < sizeBlocksJL3; j++) {
					if (blocksLevelThree[i][j] != NULL) {
						blocksLevelThree[i][j]->unsetCollision();
					}
				}
			}
		}


		void deInitLevelOne() {
			for (int i = 0; i < sizeBlocksiL1; i++) {
				for (int j = 0; j < sizeBlocksJL1; j++) {

					if (blocksLevelOne[i][j] != NULL) {
						delete blocksLevelOne[i][j];
						blocksLevelOne[i][j] = NULL;
					}

				}
			}
		}

		void deInitLevelTwo() {
			for (int i = 0; i < sizeBlocksiL2; i++) {
				for (int j = 0; j < sizeBlocksJL2; j++) {

					if (blocksLevelTwo[i][j] != NULL) {
						delete blocksLevelTwo[i][j];
						blocksLevelTwo[i][j] = NULL;
					}

				}
			}
		}

		void deInitLevelThree() {
			for (int i = 0; i < sizeBlocksiL3; i++) {
				for (int j = 0; j < sizeBlocksJL3; j++) {

					if (blocksLevelThree[i][j] != NULL) {
						delete blocksLevelThree[i][j];
						blocksLevelThree[i][j] = NULL;
					}

				}
			}
		}



		void resetLevelNumber() {
			levelCounter = 1;
		}



		void init() {

			contYL1 = 0.0f;
			contXL1 = 0.0f;

			contYL2 = 0.0f;
			contXL2 = 0.0f;

			contYL3 = 0.0f;
			contXL3 = 0.0f;

			levelCounter = 1;
			currentLevel = levelNumber::levelOne;
			myLevel = new Level();
			initLevelOne();
			initLevelTwo();
			initLevelThree();
		}

		void update() {
			cout << (int)currentLevel << endl;

			switch (currentLevel) {
			case levelNumber::levelOne:

				updateLevelOne();

				if (myLevel->getBlocksLeftL1() == 0) {
					currentLevel = levelNumber::levelTwo;
					myLevel->addlevelCounter();
					player::playerRacket->resetLives();
					gameBall->resetBallPos();
					gameBall->setShootFalse();
					player::playerRacket->resetRacketPos();
				}

				break;

			case levelNumber::levelTwo:
				updateLevelTwo();

				if (myLevel->getBlocksLeftL2() == 0) {
					currentLevel = levelNumber::levelThree;
					myLevel->addlevelCounter();

					player::playerRacket->resetLives();
					gameBall->resetBallPos();
					gameBall->setShootFalse();
					player::playerRacket->resetRacketPos();
				}

				break;

			case levelNumber::levelThree:
				updateLevelThree();				

				if (myLevel->getBlocksLeftL3() == 0) {
					currentScene = Scene::winScene;	
				}

				break;
			}
		}

		void draw() {
			switch (currentLevel) {
			case levelNumber::levelOne:
				drawLevelOne();
				break;

			case levelNumber::levelTwo:
				drawLevelTwo();
				break;

			case levelNumber::levelThree:
				drawLevelThree();
				break;
			}
		}

		void deinit() {
			deInitLevelOne();
			deInitLevelTwo();
			deInitLevelThree();
		}
	}
}