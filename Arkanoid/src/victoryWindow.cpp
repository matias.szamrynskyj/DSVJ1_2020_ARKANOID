#include "victoryWindow.h"

using namespace arkanoid;
using namespace buttons;
using namespace menu;
using namespace play;
using namespace player;
using namespace ball;
using namespace levels;


namespace arkanoid {

	namespace victoryWindow {

		int textPosX;
		int textPosY;

		int playerPointsPosX;
		int playerPointsPosY;

		float wonExitPosX;
		float wonExitPosY;
		Vector2 WonExitPos;

		void init() {
			textPosX = GetScreenWidth() / 2 - 80;
			textPosY = GetScreenHeight() / 2 - 250;

			playerPointsPosX = GetScreenWidth() / 2 - 150;
			playerPointsPosY = GetScreenHeight() / 2 - 150;

			wonExitPosX = buttonMiddleX;
			wonExitPosY = 250.0f;

			WonExitPos = { wonExitPosX , wonExitPosY };
		}

		void update() {
			exitButton->changePos(WonExitPos);
			exitButton->buttonLogic4(exitButton, collisionColor, defaultColor, currentScene, menuScreen);

			if (currentScene == menuScreen) {
				exitButton->setOriginalPos();
				playing = false;

				//Meter todo esto en una funcion
				playerRacket->resetLives();
				levels::myLevel->resetAll();
				gameBall->resetBallPos();
				gameBall->setShootFalse();
				playerRacket->resetRacketPos();
				//-------------------------------
			}
		}

		void draw() {
			exitButton->drawButton(exitButton->getColors());
			exitButton->showText(font, exitText, exitTextPausePos, quitFontSize, 10.0f, textColor);

			DrawText(FormatText("Total Points: %i", playerRacket->getPoints()), playerPointsPosX, playerPointsPosY, 40, YELLOW);			
			DrawText("You Won!!!", textPosX, textPosY, 40, YELLOW);
		}

		void deinit() {

		}
	}
}