#ifndef	VICTORYWINDOW_H
#define VICTORYWINDOW_H

#include "raylib.h"
#include "buttons.h"
#include "menu.h"
#include "play.h"
#include "player.h"
#include "ball.h"
#include "levels.h"

namespace arkanoid {

	namespace victoryWindow {

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
