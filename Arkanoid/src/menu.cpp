#include "menu.h"

using namespace arkanoid;
using namespace buttons;
using namespace scenes;

namespace arkanoid {

	namespace menu {

		//Tama�o botones
		float buttonWidth;
		float buttonHeight;

		float yesNoHeight;
		float yesNoWidth;

		float prevNextHeight;
		float prevNextWidth;

		//Posicion en pantalla
		float buttonMiddleX;
		float buttonMiddleY;

		float arkanoidLogoPosX;
		float arkanoidLogoPosY;

		float playPosX;
		float playPosY;

		float tutorialPosX;
		float tutorialPosY;

		float creditsPosX;
		float creditsPosY;

		float exitPosX;
		float exitPosY;

		float exitPausePosX;
		float exitPausePosY;

		float yesPosX;
		float yesPosY;

		float noPosX;
		float noPosY;

		float backPosX;
		float backPosY;

		float backMenuX;
		float backMenuY;

		float previousPosX;
		float previousPosY;

		float nextPosX;
		float nextPosY;

		float nextSimbolPosX;
		float nextSimbolPosY;

		float playerSelectionX;
		float playerSelectionY;

		float resetPosX;
		float resetPosY;	

		//Tama�o de la fuente
		float playFontSize;
		float tutorialFontSize;
		float creditsFontSize;
		float quitFontSize;
		float yesFontSize;
		float noFontSize;
		float backFontSize;
		float arrowFontSize;
		float p1Font;

		//Texto en los cuadrados
		char playText[5] = "PLAY";
		char tutorialText[9] = "TUTORIAL";
		char creditsText[8] = "CREDITS";
		char exitText[5] = "EXIT";
		char backText[5] = "BACK";
		char nextText[5] = "NEXT";
		char yesText[4] = "YES";
		char noText[3] = "NO";
		char p1Text[3] = "P1";
		char previousSimbol[2] = "<";
		char nextSimbol[2] = ">";
		char resetText[6] = "RESET";

		Vector2 buttonSize;
		Vector2 buttonSize2;
		Vector2 buttonSize3;
		Vector2 playerSelectButtonSize;
		

		//Posicion de los textos dentro de los carteles
		Vector2 playTextPos;
		Vector2 tutorialTextPos;
		Vector2 creditsTextPos;
		Vector2 exitTextPos;
		Vector2 exitTextPausePos;
		Vector2 yesTextPos;
		Vector2 noTextPos;
		Vector2 backTextPos;
		Vector2 previousSimbolPos;
		Vector2 nextSimbolPos;
		Vector2 pongTextPos;
		Vector2 p1SelectTextPos;
		Vector2 resetTextPos;

		//Posicion del boton exit en menu pausa
		Vector2 exitPausePos;

		Color collisionColor;
		Color defaultColor;
		Color textColor;

		Font font;
		
		int versionPosX;
		int versionPosY;
		int versionFontSize;

		void init() {
			//inicializar todas las variables
			buttonWidth = GetScreenWidth() * 0.38f;
			buttonHeight = 105.0f;

			//buttonWidth = 300.0f;
			//buttonHeight = 105.0f;

			yesNoHeight = 140.0f;
			yesNoWidth = 140.0f;

			prevNextHeight = 50.0f;
			prevNextWidth = 40.0f;			

			buttonMiddleX = static_cast<float>(GetScreenWidth()) / 2 - buttonWidth / 2;
			buttonMiddleY = static_cast<float>(GetScreenHeight()) / 2 - buttonWidth / 2;

			arkanoidLogoPosX = static_cast<float>(GetScreenWidth()) / 2 - 90;
			arkanoidLogoPosY = 100.0f;

			playPosX = buttonMiddleX;
			playPosY = 30.0f;

			tutorialPosX = buttonMiddleX;
			tutorialPosY = 165.0f;

			creditsPosX = buttonMiddleX;
			creditsPosY = 300.0f;

			exitPosX = buttonMiddleX;		
			exitPosY = 435.0f;

			exitPausePosX = buttonMiddleX;
			exitPausePosY = 250.0f;

			yesPosX = static_cast<float>(GetScreenWidth())/2 - 220;
			yesPosY = static_cast<float>(GetScreenHeight())/2 - 100;

			noPosX = yesPosX + 320;
			noPosY = yesPosY;

			backPosX = 600.0f;
			backPosY = 510.0f;

			backMenuX = 616.0f;
			backMenuY = 530.0f;

			previousPosX = 300.0f;
			previousPosY = 230.0f;

			nextPosX = backPosX;
			nextPosY = backPosY;

			nextSimbolPosX = 200;
			nextSimbolPosY = 200;

			playerSelectionX = 470.0f;
			playerSelectionY = 80.0f;

			resetPosX = buttonMiddleX;
			resetPosY = 250.0f;

			playFontSize = 105.0f;
			tutorialFontSize = 50.0f;
			creditsFontSize = 58.0f;
			quitFontSize = 105.0f;
			yesFontSize = 60.0f;
			noFontSize = 90.0f;
			backFontSize = 58.0f;
			arrowFontSize = 40.0f;
			p1Font = 30.0f;

			buttonSize = { buttonWidth ,buttonHeight };
			buttonSize2 = { yesNoWidth ,yesNoHeight };
			buttonSize3 = { prevNextWidth ,prevNextHeight };
			playerSelectButtonSize = { prevNextWidth ,prevNextHeight - 5 };


			//Posicion de los textos dentro de los carteles-------------------
			playTextPos = { playPosX + 18, playPosY + 5 };
			tutorialTextPos = { tutorialPosX + 5, tutorialPosY + 30 };
			creditsTextPos = { creditsPosX + 10 , creditsPosY + 30 };
			exitTextPos = { exitPosX + 25, exitPosY + 7 };
			exitTextPausePos = { exitPausePosX + 20, exitPausePosY + 5 };
			yesTextPos = { yesPosX + 5, yesPosY + 42 };
			noTextPos = { noPosX + 10, noPosY + 32 };
			backTextPos = { backMenuX, backMenuY };
			previousSimbolPos = { previousPosX + 14, previousPosY + 8 };
			nextSimbolPos = { nextSimbolPosX + 14, nextSimbolPosY + 8 };
			pongTextPos = { arkanoidLogoPosX, arkanoidLogoPosY };
			p1SelectTextPos = { playerSelectionX + 5, playerSelectionY + 10 };
			resetTextPos = { resetPosX , resetPosY };
			//----------------------------------------------------------------

			exitPausePos = { exitPausePosX, exitPausePosY };

			collisionColor = RED;
			defaultColor = PINK;
			textColor = BLUE;

			font = GetFontDefault();
		
			versionPosX = 30;
			versionPosY = GetScreenHeight() - 40; ;
			versionFontSize = 30;
		}

		void update() {
			playButton->buttonLogic4(playButton, collisionColor, defaultColor, currentScene, playScreen);
			tutorialButton->buttonLogic4(tutorialButton, collisionColor, defaultColor, currentScene, tutorialScreen);
			creditsButton->buttonLogic4(creditsButton, collisionColor, defaultColor, currentScene, creditsScreen);
			exitButton->buttonLogic4(exitButton, collisionColor, defaultColor, currentScene, exitScreen);
		}

		void draw() {
			EnableCursor();

			playButton->drawButton(playButton->getColors());
			playButton->showText(font, playText, playTextPos, playFontSize, 8.0f, textColor);

			tutorialButton->drawButton(tutorialButton->getColors());
			tutorialButton->showText(font, tutorialText, tutorialTextPos, tutorialFontSize, 8.0f, textColor);

			creditsButton->drawButton(creditsButton->getColors());
			creditsButton->showText(font, creditsText, creditsTextPos, creditsFontSize, 8.0f, textColor);

			exitButton->drawButton(exitButton->getColors());
			exitButton->showText(font, exitText, exitTextPos, quitFontSize, 8.0f, textColor);

			DrawText("v0.2.0", versionPosX, versionPosY, versionFontSize, GREEN);
		}

		void deinit() {

		}
	}

}