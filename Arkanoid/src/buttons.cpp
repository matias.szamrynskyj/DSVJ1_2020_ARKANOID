#include "buttons.h"

using namespace arkanoid;
using namespace menu;
using namespace scenes;
using namespace std;

namespace arkanoid {

	namespace buttons {

		Buttons::Buttons()
			:button{ button.x = 0, button.y = 0, button.width = width, button.height = height }, color{ myColor }, name{ fixedName }{
		}

		Buttons::Buttons(float x, float y, Vector2 size, Color color)
			: button{ button.x = x, button.y = y, button.width = size.x, button.height = size.y }, originalPos{ originalPos.x = button.x ,originalPos.y = button.y }, color{ color }{
		}

		Buttons::~Buttons() {
			cout << "Se borro " + name << endl;
		}

		void Buttons::setHeight(float height) {
			button.height = height;
		}
		void Buttons::setWidth(float width) {
			button.width = width;
		}
		void Buttons::setInitPos(Vector2 pos) {
			button.x = pos.x;
			button.y = pos.y;

			originalPos.x = pos.x;
			originalPos.y = pos.y;
		}
		void Buttons::setOriginalPos() {
			button.x = originalPos.x;
			button.y = originalPos.y;
		}
		void Buttons::setNewPos(Vector2 pos) {
			button.x = pos.x;
			button.y = pos.y;
		}
		void Buttons::setColors(Color& setColor) {
			color = setColor;
		}

		void Buttons::setName(std::string _name) {
			name = _name;
		}

		float Buttons::getHeight() {
			return button.height;
		}
		float Buttons::getWidth() {
			return button.width;
		}
		float Buttons::getX() {
			return button.x;
		}
		float Buttons::getY() {
			return button.y;
		}
		Rectangle Buttons::getButton() {
			return button;
		}
		Color Buttons::getColors() {
			return color;
		}
		float Buttons::getFixedSpacing() {
			return fixedSpacing;
		}

		void Buttons::changePos(Vector2 pos) {
			button.x = pos.x;
			button.y = pos.y;
		}

		void Buttons::drawButton(Color color) {
			DrawRectangleRec(button, color);
		}

		void Buttons::showText(Font font, const char* text, Vector2 position, float fontSize, float spacing, Color color) {
			DrawTextEx(font, text, position, fontSize, spacing, color);
		}

		//Logica que permite salir del juego
		void Buttons::buttonLogic1(Buttons* button, Color pressed, Color standby, bool& exitGame) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					sound::buttonSFX->playSound();
					exitGame = true;
				}
			}
			else {
				button->setColors(standby);
			}
		}

		void Buttons::buttonLogic2(Buttons* button, Color pressed, Color standby, int& changed) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					sound::buttonSFX->playSound();
					if (changed == 1) {
						changed = 2;
					}
					else {
						changed = 1;
					}
				}
			}
			else {
				button->setColors(standby);
			}
		}

		//Logica que pone el playing en true para jugar
		void Buttons::buttonLogic3(Buttons* button, Color pressed, Color standby, bool& playing) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					sound::buttonSFX->playSound();
					playing = true;
				}
			}
			else {
				button->setColors(standby);
			}
		}

		//Logica para el cambio de escenas
		void Buttons::buttonLogic4(Buttons* button, Color pressed, Color standby, Scene& currentScene, Scene goToScreen) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					sound::buttonSFX->playSound();
					currentScene = goToScreen;
				}
			}
			else {
				button->setColors(standby);
			}
		}


		void Buttons::previousButtonLogic(Buttons* button, Color pressed, Color standby, int& CounterP1) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					CounterP1--;
				}
			}
			else {
				button->setColors(standby);
			}
		}

		void Buttons::nextButtonLogic(Buttons* button, Color pressed, Color standby, int& CounterP2) {
			if (CheckCollisionPointRec(GetMousePosition(), button->getButton())) {
				button->setColors(pressed);
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					CounterP2++;
				}
			}
			else {
				button->setColors(standby);
			}
		}

		Buttons* playButton;
		Buttons* tutorialButton;
		Buttons* creditsButton;
		Buttons* exitButton;
		Buttons* yesButton;
		Buttons* noButton;
		Buttons* backButton;
		Buttons* nextButton;
		Buttons* previousSimbolButton;
		Buttons* nextSimbolButton;
		Buttons* playerSelectButton;
		Buttons* playerLeftKey;
		Buttons* playerRightKey;
		Buttons* resetButton;

		Texture2D buttonPlay;

		void init() {
			playButton = new Buttons(playPosX, playPosY, buttonSize, defaultColor);
			tutorialButton = new Buttons(tutorialPosX, tutorialPosY, buttonSize, defaultColor);
			creditsButton = new Buttons(creditsPosX, creditsPosY, buttonSize, defaultColor);
			exitButton = new Buttons(exitPosX, exitPosY, buttonSize, defaultColor);
			yesButton = new Buttons(yesPosX, yesPosY, buttonSize2, defaultColor);
			noButton = new Buttons(noPosX, noPosY, buttonSize2, defaultColor);
			backButton = new Buttons(backPosX, backPosY, buttonSize, defaultColor);
			previousSimbolButton = new Buttons(previousPosX, previousPosY, buttonSize3, defaultColor);
			nextButton = new Buttons(nextPosX, nextPosY, buttonSize, defaultColor);
			nextSimbolButton = new Buttons(nextPosX, nextPosY, buttonSize3, defaultColor);
			playerSelectButton = new Buttons(playerSelectionX, playerSelectionY, playerSelectButtonSize, defaultColor);
			resetButton = new Buttons(resetPosX, resetPosY, buttonSize, defaultColor);


			playButton->setName("play Button");
			tutorialButton->setName("option Button");
			creditsButton->setName("tutorial Button");
			exitButton->setName("exit Button");
			yesButton->setName("yes Button");
			noButton->setName("no Button");
			backButton->setName("back Button");
			previousSimbolButton->setName("previous Button");
			nextButton->setName("next Button");
			playerSelectButton->setName("player Select Button");
			resetButton->setName("reset Game");
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {
			delete playButton;
			delete tutorialButton;
			delete creditsButton;
			delete exitButton;
			delete yesButton;
			delete noButton;
			delete backButton;
			delete nextButton;
			delete previousSimbolButton;
			delete playerSelectButton;		
		}
	}
}